# oispa kaljaa screenshot
Skripti jolla otetaan joko ikkunaa klikkaamalla tai hiirellä vetämällä alueesta kuvakaappaus johon skripti generoi oletuksena "oispa kaljaa" -tekstin, tai vaihtoehtoisesti `-m` argumentilla itse kirjoitetun "meemitekstin".  

## Riippuvuudet
linuxin `scrot` pakettienhallinnastasi,  
pythonin riippuvuudet komennolla `pip install -r requirements.txt`

## käyttö
ajellaan esimerkiksi komennolla `./screenshot.py -v`,  
tai `./screenshot.py -m "foo bar"`


