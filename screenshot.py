#!/usr/bin/env python3.7
from PIL import Image, ImageDraw, ImageFont
import sys
import os
import argparse
import time

# handle arguments
argparser = argparse.ArgumentParser()
argparser.add_argument("-v", "--verbose", help="verbose output",
    action="store_true")
argparser.add_argument("-m", "--meme", help="greentext meme instead of default 'oispa kaljaa'")

args = argparser.parse_args()

meme = bool(args.meme)

screenshot_dir = '/tmp/oispakaljaa-screenshot'

if not os.path.exists(screenshot_dir):
    os.mkdir(screenshot_dir)
    if args.verbose:
        print("directory " + screenshot_dir + " created.") 
else:
    if args.verbose:
        print("directory " + screenshot_dir + " exists.")

# use unix timestamp suffix to avoid filename conflicts
image_name = 'ss_' + str(int(time.time())) + '.png'

os.system("scrot -s -e 'mv $f " + screenshot_dir +
    "/" + image_name + "'")

image = Image.open(screenshot_dir + '/' + image_name)

image_width, image_height = image.size
if args.verbose:
    print("width: " + str(image_width) + "px")
    print("height: " + str(image_height) + "px")

if image_height > image_width:
    font_size = int(image_width / 10)
else:
    font_size = int((image_height / 10))
if args.verbose:
    print("font size: " + str(font_size))

message = None
if meme:
    message = '>' + args.meme + ' :D'
else:
    message = 'oispa kaljaa'
if args.verbose:
    print("message: '" + message + "'")
    print("text size: " + str((len(message) * (font_size / 1.64))))

pos_x = (image_width - (len(message) * (font_size / 1.64))) / 2
pos_y = image_height - (image_height / 5)

while meme and pos_x < 0:
    font_size -= 1
    pos_x = (image_width - (len(message) * (font_size / 1.64))) / 2

if args.verbose:
    print("position x: " + str(pos_x))
    print("position y: " + str(pos_y))

draw = ImageDraw.Draw(image)

font = ImageFont.truetype('Hack-Regular.ttf', size=font_size)

if meme:
    color = 'rgb(120, 153, 34)' # 4chan greentext color
else:
    color = 'rgb(255, 0, 0)' 

draw.text((pos_x, pos_y), message, fill=color, font=font)

new_image = screenshot_dir + '/' + image_name
image.save(new_image)

if args.verbose:
    new_image = "file created: " + new_image

print(new_image)
